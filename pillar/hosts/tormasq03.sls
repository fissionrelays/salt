networking:
  hosts:
    10.12.144.4: saltmaster.fissionrelays.net
    10.12.144.5: vault.fissionrelays.net

roles:
  - tor

tor:
  SocksPort: 9050
  relay:
    enabled: True
    Nickname: FissionMasq03
    ExitRelay: True
    ORPort: 54998
    DirPort: 55000
