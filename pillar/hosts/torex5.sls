roles:
  - tor

firewall:
  rules:
    host:
      allow4:
        - port: 22
          proto: tcp

tor:
  SocksPort: 9050
  relay:
    enabled: True
    Nickname: FissionEx5
    ExitRelay: True
    ExitPolicyCustom:
      - 'reject 171.161.116.100:*' # abuse complaint via BH116507, expires 2022-03-27


users:
  host:
    revoked:
      - blackhost
