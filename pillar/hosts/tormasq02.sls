networking:
  hosts:
    10.12.144.4: saltmaster.fissionrelays.net
    10.12.144.5: vault.fissionrelays.net

roles:
  - tor

tor:
  SocksPort: 9050
  relay:
    enabled: True
    Nickname: FissionMasq02
    ExitRelay: True
    ORPort: 57625
    DirPort: 61328
