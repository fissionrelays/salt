install_nginx:
  pkg.installed:
    - pkgs:
      - nginx

nginx_service:
  service.running:
    - name: nginx
    - require:
      - pkg: install_nginx

/var/www/html/index.html:
  file.symlink:
    - target: /etc/tor/tor-exit-notice.html
    - require:
      - pkg: install_nginx
      - file: /etc/tor/tor-exit-notice.html

/var/www/html/index.nginx-debian.html:
  file.absent:
    - require:
      - file: /var/www/html/index.html

/etc/nginx/sites-available/default:
  file.managed:
    - user: root
    - group: root
    - mode: 644
    - source: salt://roles/tor/files/nginx-default.conf
    - require:
      - pkg: install_nginx
    - watch_in:
      - service: nginx_service
