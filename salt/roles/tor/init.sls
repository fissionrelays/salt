include:
  - roles.tor.install
  - roles.tor.config
  - roles.tor.firewall

  {% if salt['pillar.get']('tor:relay:enabled', False) -%}
  - roles.tor.nginx
  {% endif -%}
